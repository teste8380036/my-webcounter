# Webcounter Project
Simple Python Webcounter with redis server

## Requirements

- Docker account: 
- Gitlab account: 
## Project tree

```
📦webcounter
 ┣ 📂tests
 ┃ ┣ 📜test_redis.py
 ┃ ┗ 📜test_webcounter.py
 ┣ 📂webcounter
 ┃ ┣ 📂static
 ┃ ┃ ┗ 📜main.css
 ┃ ┣ 📂templates
 ┃ ┃ ┗ 📜index.html
 ┃ ┣ 📜__init__.py
 ┃ ┣ 📜__main__.py
 ┃ ┗ 📜redis_helper.py
 ┣ 📜.gitlab-ci.yml
 ┣ 📜Dockerfile
 ┣ 📜README.md
 ┣ 📜docker-compose.yml
 ┣ 📜jmeter-webcounter-tests.jmx
 ┣ 📜requirements-tests.txt
 ┗ 📜requirements.txt
```

---
## Developer tasks

### Local run

    $ python -m webcounter

### Local test
    
    $ python -m pytest tests/

### Integration tests

    docker run --name tests -it -v $PWD:/app  -w /app --rm justb4/jmeter -JURL=$(hostname -i) -n -t jmeter-webcounter-tests.jmx

### Coverage Report

    coverage run --source=webcounter -m pytest tests
    coverage report -m

### Cyclomatic complexity
    radon  cc -a -s webcounter/

### Pyinstruments benchmark

    pyinstrument webcounter/redis_helper.py

---
## Manual server operations

### Build
    docker build -t <docker-id>/webcounter:latest .

### Run Dependencies
    docker run -d  -p 6379:6379 --name redis --rm redis:alpine

### Deploy
    docker run -d --rm -p 80:5000 --name webcounter --link redis -e REDIS_URL=redis <docker-id>/webcounter:latest

---
## Cluster operations

### Push to docker repository

    docker login 
    docker push <docker-id>/webcounter:latest

### Create a docker swarm cluster

    docker swarm init

### Deploy stack app 

    docker stack deploy --compose-file docker-compose.yml app

### Verify project up and running

    docker stack ps app
---
## Automated operations CI/CD with `gitlab-runner`

![](img/20230607-090619.png)

### Instal gitlab-runner latest version on manager

```
# Download the binary for your system
sudo curl -L --output /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64

# Give it permission to execute
sudo chmod +x /usr/local/bin/gitlab-runner

```
### Get a `new project runner` from ` settings » CI/CD » Runners`

Copy/paste instructions to manager server

Example on how to register new server

```
gitlab-runner register  \
    --non-interactive \
    --url https://gitlab.com  \
    --executor "shell" \
    --tag-list "shell-build-server,shell-deploy-server" \
    --token glrt-DxjA37pyEQXXwTzmeGy3
```

### Run runner

    $ gitlab-runner run


### GitLab Variables 
Variables protect passwords and sensitive info from source code: `Gitlab » Settings » CI/CD » Variables`

Create Gitlab variables with docker credentials: `$USER` and `$PASSWORD`
 
# Pipelines

Commit files to the repository

Inspect sucess/fail in: `Gitlab » Build » Pipelines`

![](img/20230607-090544.png)